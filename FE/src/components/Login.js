import axios from 'axios';
import React, { useState } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';




function Login() {
  const [credentials, setCredentials] = useState({ uname: '', password: '' });
  const navigate = useNavigate();

 
  // function handleClick() {
  //   navigate('/RegistrationForm');
  // }
  const handleSubmit = async (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    try {
      const response = await axios.post('/user/login', credentials);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <center>
    <h1>Login</h1>
    <form onSubmit={handleSubmit}>
      <h4>Username: </h4>
      <input
        type="text"
        value={credentials.uname}
        onChange={(event) =>
          setCredentials({ ...credentials, uname: event.target.value })
        }
        id="name"
      />
  
      <h4>Password: </h4>
      <input
        type="password"
        value={credentials.password}
        onChange={(event) =>
          setCredentials({ ...credentials, password: event.target.value })
        }
        id="password"
      />
      <br />
      <br />
      <button type="submit">Sign In</button>
    </form>
    
    <p>
        Don't have an account? 
        <Link to="./RegistrationForm">Register here</Link>
      </p>
  </center>
  
  );
 }

 export default Login