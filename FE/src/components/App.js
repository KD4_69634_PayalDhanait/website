
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import About from './About';
import Contact from './Contact';
import Login from './Login';
import HandleStorage from './HandleStorage';
import Home from './Home';
import PasswordReset from './PasswordReset';
import RegistrationForm from './RegistrationForm';

function App()
{
    
   var navigate  =  useNavigate(); 
    var {isUserLoggedIn,getUserName,clearStorage}  = HandleStorage();
    const handleClick = () => {
        navigate("/Home");
      };
    
   return  <>
    
    <hr></hr>
    <Link to={"/home"}>Home</Link> | 
    <Link to={"/about"}>About Us</Link> | 
    <Link to={"/Contact"}>Contact Us</Link> |  
    <Link to={"/RegistrationForm"}>RegistrationForm</Link> |
    <hr></hr>
   <nav>
   <Routes>
         <Route exact path="/" element={<Home/>}></Route>
         <Route exact path="/home" element={<Home/>}></Route>
        <Route exact path="/about" element={<About/>}></Route>
         <Route exact path="/Contact" element={<Contact/>}></Route> 
        <Route exact path="/RegistrationForm" element={<RegistrationForm/>}></Route>
        <Route  path="/Login" element={<Login/>}></Route>
        <Route exact path="/PasswordReset" element={<PasswordReset/>}></Route>
       
    </Routes>
   </nav>
    <button onClick={handleClick}>Click me</button>
    <hr></hr>
</>
}

export default App
