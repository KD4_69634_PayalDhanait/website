import RegistrationForm from "./RegistrationForm";

import { Route, Routes, useNavigate } from 'react-router-dom';

 


function Home() {
 
   
  
    return (
      <center>
      <h1>Home Page</h1>
      <Routes>
      <Route exact path="/" element={<Home />} />
      <Route exact path="/RegistrationForm" element={<RegistrationForm/>}>RegistrationForm</Route>
    </Routes>
    </center>
    
    );
   }
  
   export default Home